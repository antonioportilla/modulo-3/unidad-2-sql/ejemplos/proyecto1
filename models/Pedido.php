<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedido".
 *
 * @property int $IdPedido
 * @property string $IdCliente
 * @property int $IdEmpleado
 * @property string $FechaPedido
 * @property string $FechaEntrega
 * @property string $FechaEnvio
 * @property int $IdEmpresasTransporte
 * @property double $Cargo
 * @property string $Destinatario
 * @property string $DireccionDestinatario
 * @property string $CiudadDestinatario
 * @property string $RegionDestinatario
 * @property string $CodPostalDestinatario
 * @property string $PaisDestinatario
 * @property double $total
 *
 * @property DetallesDePedido[] $detallesDePedidos
 * @property Producto[] $productos
 * @property Ivas[] $ivas
 * @property Cliente $cliente
 * @property Empleado $empleado
 * @property Empresastransporte $empresasTransporte
 */
class Pedido extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdEmpleado', 'IdEmpresasTransporte'], 'integer'],
            [['FechaPedido', 'FechaEntrega', 'FechaEnvio'], 'safe'],
            [['Cargo', 'total'], 'number'],
            [['IdCliente'], 'string', 'max' => 10],
            [['Destinatario'], 'string', 'max' => 80],
            [['DireccionDestinatario'], 'string', 'max' => 120],
            [['CiudadDestinatario', 'RegionDestinatario', 'PaisDestinatario'], 'string', 'max' => 30],
            [['CodPostalDestinatario'], 'string', 'max' => 20],
            [['IdCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['IdCliente' => 'IdCliente']],
            [['IdEmpleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['IdEmpleado' => 'IdEmpleado']],
            [['IdEmpresasTransporte'], 'exist', 'skipOnError' => true, 'targetClass' => Empresastransporte::className(), 'targetAttribute' => ['IdEmpresasTransporte' => 'IdEmpresasTransporte']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdPedido' => 'Id Pedido',
            'IdCliente' => 'Id Cliente',
            'IdEmpleado' => 'Id Empleado',
            'FechaPedido' => 'Fecha Pedido',
            'FechaEntrega' => 'Fecha Entrega',
            'FechaEnvio' => 'Fecha Envio',
            'IdEmpresasTransporte' => 'Id Empresas Transporte',
            'Cargo' => 'Cargo',
            'Destinatario' => 'Destinatario',
            'DireccionDestinatario' => 'Direccion Destinatario',
            'CiudadDestinatario' => 'Ciudad Destinatario',
            'RegionDestinatario' => 'Region Destinatario',
            'CodPostalDestinatario' => 'Cod Postal Destinatario',
            'PaisDestinatario' => 'Pais Destinatario',
            'total' => 'Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetallesDePedidos()
    {
        return $this->hasMany(DetallesDePedido::className(), ['IdPedido' => 'IdPedido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['IdProducto' => 'IdProducto'])->viaTable('detalles_de_pedido', ['IdPedido' => 'IdPedido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvas()
    {
        return $this->hasMany(Ivas::className(), ['idpedido' => 'IdPedido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['IdCliente' => 'IdCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleado::className(), ['IdEmpleado' => 'IdEmpleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresasTransporte()
    {
        return $this->hasOne(Empresastransporte::className(), ['IdEmpresasTransporte' => 'IdEmpresasTransporte']);
    }
}
